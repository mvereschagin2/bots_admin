App.chatChannel = App.cable.subscriptions.create { channel: "BotChannel", bot: "mikkies_bot" },
  received: (data) ->
    console.log(data)
    @appendLine(data)

  appendLine: (data) ->
    html = @createLine(data)
    $("#msg_history#{data['chat_id']}").append(html)

  createLine: (data) ->
    """
    <div class="incoming_msg">
      <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
      <div class="received_msg">
        <div class="received_withd_msg">
          <p>#{data['body']}</p>
          <span class="time_date">#{data['date']}</span></div>
      </div>
    </div>
    """

$(document).ready ->
  $(".msg_send_btn").click ->
    chat_id = this.attributes['chat_id'].value
    bot     = this.attributes['bot'].value
    message = $(this).prev().val()
    App.chatChannel.send({ bot: bot, chat_id: chat_id, body: message })