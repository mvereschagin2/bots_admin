class BotChannel < ApplicationCable::Channel

  def subscribed
    stream_from "#{params[:bot]}_updates"
  end

  def receive(data)
    message =  Telegram.bots[data['bot'].to_sym].send_message(chat_id: data['chat_id'], text: data['body'])['result'] rescue nil
    Telegram::MessageUpdater.new(data['bot']).store_updates!([{'message' => message}])
  end

end