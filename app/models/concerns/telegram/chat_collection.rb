module Telegram
  class ChatCollection

    include Storage

    attr_reader :title

    def initialize(title)
      @title = title
    end

    def call
      Oj.load(storage_data('history')).map{|item| item['message']}.compact.each do |message|
        chat = chats.detect{|chat| chat.id == message['chat']['id']}
        unless chat
          chat = Chat.new(message['chat']['id'])
          chats << chat
        end
        chat.add_message(message)
      end
      chats
    end

    def chats
      @chats ||= []
    end

    class Chat

      attr_reader :id

      def initialize(id)
        @id = id
        @messages = []
      end

      def username
        msg = messages.select{|m| m['from']['is_bot'] == false}.last
        msg['from']['first_name'] if msg
      end

      def add_message(message)
        @messages << message
      end

      def messages
        @messages.sort_by{|m| m['date']}
      end

    end
  end
end
