module Telegram
  class MessageUpdater

    include Storage

    attr_reader :title, :updates

    def initialize(title)
      @title = title
    end

    def get_updates!(data=[])
      if client
        @updates = data.present? ? data : client.get_updates(offset: offset)['result']
        if @updates.blank?
          puts "none data"
        else
          if get_history.blank?
            set_storage_data('history', Oj.dump(@updates))
          else
            update_history!
          end
          notify_stream!
          @updates
        end
      end
    end

    def store_updates!(data)
      return unless data.present?
      get_updates!(data)
    end

    def reset_history_storage!
      set_storage_data('history', nil)
    end

    def get_history
      Oj.load storage_data('history')
    end

    private

    def notify_stream!
      puts "Notify #{@updates}"
      puts '-------------'
      p @updates
      @updates.map{|item| item['message']}.each do |message|
         puts ActionCable.server.broadcast(
            "#{@title}_updates",
            chat_id: message['chat']['id'],
            body: message['text'],
            date: Time.at(message['date'])
        )
      end
    end

    def update_history!
      if @updates
        updated_history_data = Oj.load(storage_data('history')) + @updates
        set_storage_data('history', Oj.dump(updated_history_data))
        set_last_updated!(@updates.map{|i| i['update_id']}.sort.last)
      end
    end

    def set_last_updated!(val)
      set_storage_data('last_updated', val)
    end

    def offset
      (storage_data('last_updated').try(:to_i) || 0) + 1
    end

    def client
      @client ||= Telegram.bots[@title.to_sym]
    end


  end
end
