Telegram::Bot::Client.prepend Module.new {
  def initialize(**)
    super
    @client = HTTPClient.new(proxy)
  end

  def proxy
    #185.62.189.17:18884
    "http://#{address}"
  end

  def address
    '81.236.223.19:53281'
  end
}