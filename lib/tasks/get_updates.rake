namespace :telegram do
  task get_updates: :environment do
    bots = Rails.application.secrets[:telegram][:bots].keys
    loop do
      bots.each do |bot_title|
        begin
          Telegram::MessageUpdater.new(bot_title).get_updates!
        rescue => e
          Rails.logger.info e.message
          Rails.logger.info e.backtrace.join("/n")
        end
      end
      sleep 1
    end
  end
end
